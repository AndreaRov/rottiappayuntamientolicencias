-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2017 a las 05:22:39
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestlic`
--
CREATE DATABASE IF NOT EXISTS `gestlic` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gestlic`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interesado`
--

CREATE TABLE `interesado` (
  `dni_int` char(9) NOT NULL,
  `nombre` char(30) NOT NULL,
  `apellido` char(50) NOT NULL,
  `razonsocial` char(50) NOT NULL,
  `direccion` char(80) NOT NULL,
  `municipio` char(30) NOT NULL,
  `codigopostal` char(5) NOT NULL,
  `telefono` char(15) NOT NULL,
  `movil` char(15) NOT NULL,
  `fax` char(15) NOT NULL,
  `correo` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `interesado`
--

INSERT INTO `interesado` (`dni_int`, `nombre`, `apellido`, `razonsocial`, `direccion`, `municipio`, `codigopostal`, `telefono`, `movil`, `fax`, `correo`) VALUES
('00000023T', 'Micael', 'Ordóñez García', 'Matrix Design', 'Carrer de Roger de Llúria', 'Barcelona', '28014', '954881268', '654881261', '954881851', 'MicaelOrdonezGarcia@gustr.com'),
('014787441', 'Laura', 'Carrera Barrera', 'Seamans Furniture', 'Crta. Cádiz-Málaga, 1', 'Ares del Maestre', '12165', '937923672', '658745328', '937653672', 'LauraCarreraBarrera@gustr.com'),
('14213421m', 'Andrea', 'Valenciano', 'Bacon', 'calle limones', 'alpedrete', '22011', '98743622', '653346876', '987654321', '@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representante`
--

CREATE TABLE `representante` (
  `dni_rep` char(9) NOT NULL,
  `nombre` char(50) NOT NULL,
  `apellido` char(50) NOT NULL,
  `razonsocial` char(50) NOT NULL,
  `direccion` char(50) NOT NULL,
  `municipio` char(30) NOT NULL,
  `codigopostal` char(5) NOT NULL,
  `telefono` char(15) NOT NULL,
  `movil` char(15) NOT NULL,
  `fax` char(30) NOT NULL,
  `correo` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `representante`
--

INSERT INTO `representante` (`dni_rep`, `nombre`, `apellido`, `razonsocial`, `direccion`, `municipio`, `codigopostal`, `telefono`, `movil`, `fax`, `correo`) VALUES
('4111412g', 'Paula', 'Valenciano', 'Bacon', 'calle limones', 'alpedrete', '22011', '98743622', '653346876', '987654321', '@gmail.com'),
('54876952J', 'Albana', 'Escobar Correa', 'Golden Joy', 'C/ Cañada del Rosal, 93', 'Cartaya', '21450', '698752468', '621542875', '965874258', 'Albana97@gmail.com'),
('6587458IM', 'Diego', 'Mota Gaona', 'Monmax', 'Ctra. de Siles, 22', 'Cáceres', '27325', '968754254', '689542013', '965874523', 'diego97lo@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `num_sol` char(15) NOT NULL,
  `fsolicitud` date NOT NULL,
  `finicio` date NOT NULL,
  `tiposuelo` char(20) NOT NULL,
  `rcatastral` char(255) NOT NULL,
  `emplazamiento` char(60) NOT NULL,
  `estadolicencia` char(200) NOT NULL,
  `tipoactiv` char(100) NOT NULL,
  `cuota` char(15) NOT NULL,
  `descripactiv` varchar(500) NOT NULL,
  `cdni` tinyint(1) NOT NULL,
  `cimpuestos` tinyint(1) NOT NULL,
  `cfotos` tinyint(1) NOT NULL,
  `cescritura` tinyint(1) NOT NULL,
  `cpago` tinyint(1) NOT NULL,
  `cmemoriaact` tinyint(1) NOT NULL,
  `cplanosloc` tinyint(1) NOT NULL,
  `clicobra` tinyint(1) NOT NULL,
  `cmodelotec` tinyint(1) NOT NULL,
  `ccolegio` tinyint(1) NOT NULL,
  `cotras` tinyint(1) NOT NULL,
  `cescrituract` tinyint(1) NOT NULL,
  `cplanosct` tinyint(1) NOT NULL,
  `clicenciasct` tinyint(1) NOT NULL,
  `cnexpect` tinyint(1) NOT NULL,
  `dni_int` char(9) NOT NULL,
  `dni_rep` char(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `solicitud`
--

INSERT INTO `solicitud` (`num_sol`, `fsolicitud`, `finicio`, `tiposuelo`, `rcatastral`, `emplazamiento`, `estadolicencia`, `tipoactiv`, `cuota`, `descripactiv`, `cdni`, `cimpuestos`, `cfotos`, `cescritura`, `cpago`, `cmemoriaact`, `cplanosloc`, `clicobra`, `cmodelotec`, `ccolegio`, `cotras`, `cescrituract`, `cplanosct`, `clicenciasct`, `cnexpect`, `dni_int`, `dni_rep`) VALUES
('1241', '2017-06-01', '2017-06-16', 'Suelo Urbano', '124512512', 'madrid', 'En Trámite', 'Cajas de Ahorro, Bancos, Entidades Financieras', '500', 'sese', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, '14213421m', '4111412g'),
('548965752955', '2017-01-09', '2017-03-08', 'Suelo Urbano', '265484515145158151815181', 'Madrid', 'En trámite', 'Hoteles, Hostales y Pensiones', '1200', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, '00000023T', '54876952J'),
('5841268741', '2016-01-19', '2016-03-09', 'Suelo Rústico', '6581485625', 'Madrid', 'Caducada', 'Actividades en suelo calificado como industrial', '2540', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.   ', 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, '014787441', '6587458IM');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `interesado`
--
ALTER TABLE `interesado`
  ADD PRIMARY KEY (`dni_int`);

--
-- Indices de la tabla `representante`
--
ALTER TABLE `representante`
  ADD PRIMARY KEY (`dni_rep`);

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`num_sol`),
  ADD KEY `dni_int` (`dni_int`),
  ADD KEY `dni_rep` (`dni_rep`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD CONSTRAINT `solicitud_ibfk_1` FOREIGN KEY (`dni_rep`) REFERENCES `representante` (`dni_rep`),
  ADD CONSTRAINT `solicitud_ibfk_2` FOREIGN KEY (`dni_int`) REFERENCES `interesado` (`dni_int`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
